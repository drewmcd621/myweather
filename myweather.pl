# My Weather
#
# Drew McDermott
# adm75@pitt.edu

$_airport = "PIT";
$_year = "2012";
$_month = "7";

$_avg = '';
$_hi = '';
$_lo = '';
$_con = '';

use Getopt::Long;

GetOptions ('airport=s' => \$_airport,
			'year=i' => \$_year,
			'month=i' => \$_month,
			'average' => \$_avg
			);






if($_avg)
{
	avgTemp($_airport,$_year,$_month);
}


#Get days in month

#Arg	0: Month
sub daysInMonth
{
my @daysofmonth = (31,-1,31,30,31,30,31,31,30,31,30,31);

my $days = $daysofmonth[$_[0]-1];

	if($days == -1)
	{
		if($_year % 4 == 0)
		{
			$days = 29;
		}
		else
		{
			$days = 28;
		}
		
	}
	return $days;
}

#get data for 1 day
#Arg 	0: Airport
#		1: Year
#		2: Month
#		3: Day
sub getDayData
{
use LWP::Simple;
	my $content = get("http://www.wunderground.com/history/airport/$_[0]/$_[1]/$_[2]/$_[3]/DailyHistory.html?format=1");
	die "Error contacting website" unless defined $content;
	
	return $content;
	
}
#Parse daily data
#Arg	0: Data
sub parseData
{
	my $data = $_[0];
	
	my @dataRows = split("<br />\n",$data);
	
	my $i =0;
	foreach $row (@dataRows)
	{
		my @tempRow = split(",",$row);
		$pData[$i] = \@tempRow;
		$i++;
	}
	my $ret = \@pData;
	checkData($ret);
	return $ret;
	
}
#check data
#Arg 	0: Parsed data
sub checkData
{
	my $pData = $_;

	if($pData[1][0] eq "No daily or hourly history data available")
	{
		die "No data availible for this date";
	}
	
}
#get average temp for month, print results
#Arg 	0: Airport
#		1: Year
#		2: Month
sub avgTemp
{
	
	
	
}



#No daily or hourly history data available


